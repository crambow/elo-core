/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.settings;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SettingsTest {

    @Test
    public void testReadEncryptWrite() throws Exception {

        File file = File.createTempFile("_test", ".json");
        file.deleteOnExit();
        Files.copy(getClass().getResourceAsStream("/root.json"), file.toPath(), StandardCopyOption.REPLACE_EXISTING);

        TestSettings settings = new TestSettings();
        settings.read(file.toPath());

        Assertions.assertNotEquals("ABC98765", settings.getRoot().getPassword());
        Assertions.assertTrue(settings.getRoot().getPassword().startsWith("{"));
        Assertions.assertTrue(settings.getRoot().getPassword().endsWith("}"));

        Assertions.assertFalse(settings.hasUnprotectedValues());

    }

}
