/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import de.elomagic.core.wbxml.codepages.CodePages;
import de.elomagic.core.wbxml.dto.FolderSync;

/**
 *
 * @author Carsten Rambow
 */
public class WbXmlEncoderTest {

    @Test
    public void testEncode() throws Exception {
        byte[] bytes = WbXmlEncoder.encode(getClass().getResourceAsStream("/sample-foldersync.xml"), CodePages.getCodePages());

        Assertions.assertArrayEquals(new byte[] {0x03, 0x01, 0x6a, 0x00, 0x00, 0x07, 0x56, 0x52, 0x03, 0x30, 0x00, 0x01, 0x01}, bytes);
    }

    @Test
    public void testEncodeObject() throws Exception {
        FolderSync fd = new FolderSync();
        fd.setSyncKey("0");

        byte[] bytes = WbXmlEncoder.encodeObject(fd, CodePages.getCodePages());

        Assertions.assertArrayEquals(new byte[] {0x03, 0x01, 0x6a, 0x00, 0x00, 0x07, 0x56, 0x52, 0x03, 0x30, 0x00, 0x01, 0x01}, bytes);
    }

}
