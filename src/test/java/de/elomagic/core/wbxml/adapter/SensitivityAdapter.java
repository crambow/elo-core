/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import de.elomagic.core.wbxml.dto.Sensitivity;

/**
 *
 * @author Carsten Rambow
 */
public class SensitivityAdapter extends XmlAdapter<String, Sensitivity> {

    @Override
    public Sensitivity unmarshal(String value) throws Exception {
        return Sensitivity.parse(value);
    }

    @Override
    public String marshal(Sensitivity value) throws Exception {
        return value == null ? null : Integer.toString(value.getValue());
    }

}
