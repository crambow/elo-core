/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml.adapter;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Adapter to convert "20180417T130000Z" into {@link ZonedDateTimeAdapter} and reverse.
 *
 * @author Carsten Rambow
 */
public class ZonedDateTimeAdapter extends XmlAdapter<String, ZonedDateTime> {

    private static final String PATTERN = "yyyyMMdd'T'HHmmssX";

    @Override
    public ZonedDateTime unmarshal(String date) throws Exception {
        return date == null ? null : ZonedDateTime.parse(date, DateTimeFormatter.ofPattern(PATTERN));
    }

    @Override
    public String marshal(ZonedDateTime zdt) throws Exception {
        return zdt == null ? null : zdt.format(DateTimeFormatter.ofPattern(PATTERN));
    }

}
