/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml.codepages;

import java.util.HashMap;
import java.util.Map;

import de.elomagic.core.wbxml.AbstractCodePage;

/**
 *
 * @author Carsten Rambow
 */
public final class SettingsCodePage extends AbstractCodePage {

    public static final int CODE_PAGE_ID = 18;

    @Override
    public int getCodePageId() {
        return CODE_PAGE_ID;
    }

    @Override
    public Map<Integer, String> getTagMap() {
        Map<Integer, String> result = new HashMap<>();

        result.put(0x05, "Settings"); // 0x05
        result.put(0x06, "Status"); // 0x06
        result.put(0x07, "Get"); // 0x07
        result.put(0x08, "Set"); // 0x08
        result.put(0x09, "Oof"); // 0x09
        result.put(0x0a, "OofState"); // 0x0A
        result.put(0x0b, "StartTime"); // 0x0B
        result.put(0x0c, "EndTime"); // 0x0C
        result.put(0x0d, "OofMessage"); // 0x0D
        result.put(0x0e, "AppliesToInternal"); // 0x0E
        result.put(0x0f, "AppliesToExternalKnown"); // 0x0F
        result.put(0x10, "AppliesToExternalUnknown"); // 0x10
        result.put(0x11, "Enabled"); // 0x11
        result.put(0x12, "ReplyMessage"); // 0x12
        result.put(0x13, "BodyType"); // 0x13
        result.put(0x14, "DevicePassword"); // 0x14
        result.put(0x15, "Password"); // 0x15
        result.put(0x16, "DeviceInformation"); // 0x16
        result.put(0x17, "Model"); // 0x17
        result.put(0x18, "IMEI"); // 0x18
        result.put(0x19, "FriendlyName"); // 0x19
        result.put(0x1a, "OS"); // 0x1A
        result.put(0x1b, "OSLanguage"); // 0x1B
        result.put(0x1c, "PhoneNumber"); // 0x1C
        result.put(0x1d, "UserInformation"); // 0x1D
        result.put(0x1e, "EmailAddresses"); // 0x1E
        result.put(0x1f, "SMTPAddress"); // 0x1F
        result.put(0x20, "UserAgent"); // 0x20
        result.put(0x21, "EnableOutboundSMS"); // 0x21
        result.put(0x22, "MobileOperator"); // 0x22
        result.put(0x23, "PrimarySmtpAddress"); // 0x23
        result.put(0x24, "Accounts"); // 0x24
        result.put(0x25, "Account"); // 0x25
        result.put(0x26, "AccountId"); // 0x26
        result.put(0x27, "AccountName"); // 0x27
        result.put(0x28, "UserDisplayName"); // 0x28
        result.put(0x29, "SendDisabled"); // 0x29
        //result.put(0x,"unknown"); // Missing 0x2a
        result.put(0x2b, "RightsManagementInformation"); // 0x2B

        return result;
    }
}
