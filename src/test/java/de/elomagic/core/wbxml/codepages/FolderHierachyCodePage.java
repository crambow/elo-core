/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml.codepages;

import java.util.HashMap;
import java.util.Map;

import de.elomagic.core.wbxml.AbstractCodePage;

/**
 *
 * @author Carsten Rambow
 */
public final class FolderHierachyCodePage extends AbstractCodePage {

    public static final int CODE_PAGE_ID = 7;

    @Override
    public int getCodePageId() {
        return CODE_PAGE_ID;
    }

    @Override
    public Map<Integer, String> getTagMap() {
        Map<Integer, String> result = new HashMap<>();

        result.put(0x05, "Folders"); // 0x05
        result.put(0x06, "Folder"); // 0x06
        result.put(0x07, "DisplayName"); // 0x07
        result.put(0x08, "ServerId"); // 0x08
        result.put(0x09, "ParentId"); // 0x09
        result.put(0x0a, "Type"); // 0x0A
        //result.put(0x0b, "Response"); // Missing 0x0B
        result.put(0x0c, "Status"); // 0x0C
        //result.put(0x0d, "ContentClass"); // Missing 0x0D
        result.put(0x0e, "Changes"); // 0x0E
        result.put(0x0f, "Add"); // 0x0F
        result.put(0x10, "Delete"); // 0x10
        result.put(0x11, "Update"); // 0x11
        result.put(0x12, "SyncKey"); // 0x12
        result.put(0x13, "FolderCreate"); // 0x13
        result.put(0x14, "FolderDelete"); // 0x14
        result.put(0x15, "FolderUpdate"); // 0x15
        result.put(0x16, "FolderSync"); // 0x16
        result.put(0x17, "Count"); // 0x17
        result.put(0x18, "Version"); // 0x18

        return result;
    }
}
