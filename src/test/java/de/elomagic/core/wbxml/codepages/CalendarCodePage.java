/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml.codepages;

import java.util.HashMap;
import java.util.Map;

import de.elomagic.core.wbxml.AbstractCodePage;

/**
 *
 * @author Carsten Rambow
 */
public final class CalendarCodePage extends AbstractCodePage {

    public static final int CODE_PAGE_ID = 4;

    @Override
    public int getCodePageId() {
        return CODE_PAGE_ID;
    }

    @Override
    public Map<Integer, String> getTagMap() {
        Map<Integer, String> result = new HashMap<>();

        result.put(0x05, "Timezone");
        result.put(0x06, "AllDayEvent");
        result.put(0x07, "Attendees");
        result.put(0x08, "Attendee");
        result.put(0x09, "Email");
        result.put(0x0a, "Name");
        result.put(0x0b, "Body");
        result.put(0x0c, "BodyTruncated");
        result.put(0x0d, "BusyStatus");
        result.put(0x0e, "Categories");
        result.put(0x0f, "Category");
        result.put(0x10, "Compressed_RTF");
        result.put(0x11, "DtStamp");
        result.put(0x12, "EndTime");
        result.put(0x13, "Exception");
        result.put(0x14, "Exceptions");
        result.put(0x15, "Deleted");
        result.put(0x16, "ExceptionStartTime");
        result.put(0x17, "Location");
        result.put(0x18, "MeetingStatus");
        result.put(0x19, "OrganizerEmail");
        result.put(0x1a, "OrganizerName");
        result.put(0x1b, "Recurrence");
        result.put(0x1c, "Type");
        result.put(0x1d, "Until");
        result.put(0x1e, "Occurrences");
        result.put(0x1f, "Interval");
        result.put(0x20, "DayOfWeek");
        result.put(0x21, "DayOfMonth");
        result.put(0x22, "WeekOfMonth");
        result.put(0x23, "MonthOfYear");
        result.put(0x24, "Reminder");
        result.put(0x25, "Sensitivity");
        result.put(0x26, "Subject");
        result.put(0x27, "StartTime");
        result.put(0x28, "UID");
        result.put(0x29, "AttendeeStatus");
        result.put(0x2a, "AttendeeType");
//        "", // b
//        "", // c
//        "", // d
//        "", // e
//        "", // f
//        "", // 0
//        "", // 1
//        "", // 2
        result.put(0x33, "DisallowNewTimeProposal");
        result.put(0x34, "ResponseRequested");
        result.put(0x35, "AppointmentReplyTime");
        result.put(0x36, "ResponseType");
        result.put(0x37, "CalendarType");
        result.put(0x38, "IsLeapMonth");
        result.put(0x39, "FirstDayOfWeek");
        result.put(0x3a, "OnlineMeetingConfLink");
        result.put(0x3b, "OnlineMeetingExternalLink");
        result.put(0x3c, "ClientUid");

        return result;
    }

}
