/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml.codepages;

import java.util.HashMap;
import java.util.Map;

import de.elomagic.core.wbxml.AbstractCodePage;

/**
 *
 * @author Carsten Rambow
 */
public class AirSyncCodePage extends AbstractCodePage {

    public static final int CODE_PAGE_ID = 0;

    @Override
    public int getCodePageId() {
        return CODE_PAGE_ID;
    }

    @Override
    public Map<Integer, String> getTagMap() {
        Map<Integer, String> result = new HashMap<>();

        result.put(0x05, "Sync");
        result.put(0x06, "Responses");
        result.put(0x07, "Add");
        result.put(0x08, "Change");
        result.put(0x09, "Delete");
        result.put(0x0a, "Fetch");
        result.put(0x0b, "SyncKey");
        result.put(0x0c, "ClientId");
        result.put(0x0d, "ServerId");
        result.put(0x0e, "Status");
        result.put(0x0f, "Collection");
        result.put(0x10, "Class");
        //result.put(0x11, "Version");
        result.put(0x12, "CollectionId");
        result.put(0x13, "GetChanges");
        result.put(0x14, "MoreAvailable");
        result.put(0x15, "WindowSize");
        result.put(0x16, "Commands");
        result.put(0x17, "Options");
        result.put(0x18, "FilterType");
        result.put(0x19, "Truncation");
        //result.put(0x1a, "RTFTruncation", // Missing 0x1A
        result.put(0x1b, "Conflict");
        result.put(0x1c, "Collections");
        result.put(0x1d, "ApplicationData");
        result.put(0x1e, "DeletesAsMoves");
        //result.put(0x1f, "NotifyGUID");
        result.put(0x20, "Supported");
        result.put(0x21, "SoftDelete");
        result.put(0x22, "MIMESupport");
        result.put(0x23, "MIMETruncation");
        result.put(0x24, "Wait");
        result.put(0x25, "Limit");
        result.put(0x26, "Partial");
        result.put(0x27, "ConversationMode");
        result.put(0x28, "MaxItems");
        result.put(0x29, "HeartbeatInterval");

        return result;
    }
}
