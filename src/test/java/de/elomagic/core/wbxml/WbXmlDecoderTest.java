/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import de.elomagic.core.wbxml.codepages.CodePages;

public class WbXmlDecoderTest {

    @Test
    public void testDecodeWbXmlToXml() throws Exception {
        byte[] bytes = WbXmlDecoder.decodeWbXmlToXml(new ByteArrayInputStream(new byte[] {0x03, 0x01, 0x6a, 0x00, 0x00, 0x07, 0x56, 0x52, 0x03, 0x30, 0x00, 0x01, 0x01}), CodePages.getCodePages());

        System.out.println("XML=" + new String(bytes));

        byte[] expected = "<?xml version='1.0' encoding='UTF-8' ?><FolderSync><SyncKey>0</SyncKey></FolderSync>".getBytes(StandardCharsets.UTF_8);

        Assertions.assertArrayEquals(expected, bytes);
    }

}
