/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml.dto;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import de.elomagic.core.wbxml.adapter.BooleanAdapter;
import de.elomagic.core.wbxml.adapter.BusyStatusAdapter;
import de.elomagic.core.wbxml.adapter.SensitivityAdapter;
import de.elomagic.core.wbxml.adapter.ZonedDateTimeAdapter;

/**
 *
 * @author Carsten Rambow
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ApplicationData {

    // <UID>7F31817FDAAB42899766C681F56FFCAA</UID>
    @XmlElement(name = "UID")
    private String uid;
    // <AllDayEvent>0</AllDayEvent>
    @XmlElement(name = "AllDayEvent")
    private Integer allDayEvent;
    // <BodyTruncated>1</BodyTruncated>
    @XmlElement(name = "BodyTruncated")
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    private Boolean bodyTruncated;
    // <BusyStatus>2</BusyStatus>
    @XmlElement(name = "BusyStatus")
    @XmlJavaTypeAdapter(BusyStatusAdapter.class)
    private BusyStatus busyStatus;
    // Chapter 2.2.2.18 <DtStamp>20171012T075006Z</DtStamp>
    @XmlElement(name = "DtStamp")
    @XmlJavaTypeAdapter(ZonedDateTimeAdapter.class)
    private ZonedDateTime dtStamp;
    // Chapter 2.2.2.20 <EndTime>20180417T130000Z</EndTime>
    @XmlElement(name = "EndTime")
    @XmlJavaTypeAdapter(ZonedDateTimeAdapter.class)
    private ZonedDateTime endTime;
    // Chapter 2.2.2.27 <Location>Room 42</Location>
    @XmlElement(name = "Location")
    private String location;
    // Chapter 2.2.2.28 <MeetingStatus>1</MeetingStatus>
    @XmlElement(name = "MeetingStatus")
    private Integer meetingStatus;
    // Chapter 2.2.2.41 <Sensitivity>0</Sensitivity>
    @XmlElement(name = "Sensitivity")
    @XmlJavaTypeAdapter(SensitivityAdapter.class)
    private Sensitivity sensitivity;
    // Chapter 2.2.2.42 <StartTime>20180417T110000Z</StartTime>
    @XmlElement(name = "StartTime")
    @XmlJavaTypeAdapter(ZonedDateTimeAdapter.class)
    private ZonedDateTime startTime;
    // Chapter 2.2.2.43 <Subject>It Meeting Time</Subject>
    @XmlElement(name = "Subject")
    private String subject;
    // Chapter 2.2.2.4 <Timezone>xP///wAAAAA.....
    @XmlElement(name = "Timezone")
    private String timezone;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Integer getAllDayEvent() {
        return allDayEvent;
    }

    public void setAllDayEvent(Integer allDayEvent) {
        this.allDayEvent = allDayEvent;
    }

    public Boolean getBodyTruncated() {
        return bodyTruncated;
    }

    public void setBodyTruncated(Boolean bodyTruncated) {
        this.bodyTruncated = bodyTruncated;
    }

    public BusyStatus getBusyStatus() {
        return busyStatus;
    }

    public void setBusyStatus(BusyStatus busyStatus) {
        this.busyStatus = busyStatus;
    }

    public ZonedDateTime getDtStamp() {
        return dtStamp;
    }

    public void setDtStamp(ZonedDateTime dtStamp) {
        this.dtStamp = dtStamp;
    }

    public ZonedDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(ZonedDateTime endTime) {
        this.endTime = endTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getMeetingStatus() {
        return meetingStatus;
    }

    public void setMeetingStatus(Integer meetingStatus) {
        this.meetingStatus = meetingStatus;
    }

    public Sensitivity getSensitivity() {
        return sensitivity;
    }

    public void setSensitivity(Sensitivity sensitivity) {
        this.sensitivity = sensitivity;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @XmlElement(name = "Attendee")
    @XmlElementWrapper(name = "Attendees")
    private List<Attendee> attendees = new ArrayList<>();

//    <Attendees>
//        <Attendee>
//            <Email>userh&#64;company.com</Email>
//            <Name>John Doe</Name>
//            <AttendeeStatus>3</AttendeeStatus>
//        </Attendee>
//    <Categories>
//        <Category>Meeting</Category>
//    </Categories>
    public List<Attendee> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Attendee> attendees) {
        this.attendees = attendees;
    }

}
