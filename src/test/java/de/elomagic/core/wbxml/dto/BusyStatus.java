/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml.dto;

import java.util.Arrays;

/**
 * Data type "BusyStatus".
 *
 * @author Carsten Rambow
 */
public enum BusyStatus {

    Free(0),
    Tentative(1),
    Busy(2),
    OutOfOffice(3),
    WorkingElsewhere(4);

    private final int value;

    private BusyStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static BusyStatus parse(String value) {
        BusyStatus result = Arrays
                .stream(values())
                .filter(item->Integer.toString(item.getValue()).equals(value))
                .findFirst()
                .orElse(null);

        return result;
    }

}
