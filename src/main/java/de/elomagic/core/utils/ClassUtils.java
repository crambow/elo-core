/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.utils;

/**
 *
 * @author Carsten Rambow
 */
public final class ClassUtils {

    private ClassUtils() {
    }

    /**
     * Check if the given class is a primitive or wrapper class of an primitive data type.
     *
     * @param clazz Class to be checked
     * @return Returns true when given class is a primitive class
     * @see Class#isPrimitive()
     */
    public static boolean isPrimitiveType(Class<?> clazz) {
        return clazz.isPrimitive()
                       || clazz == Boolean.class
                       || clazz == Character.class
                       || clazz == Byte.class
                       || clazz == Short.class
                       || clazz == Integer.class
                       || clazz == Long.class
                       || clazz == Float.class
                       || clazz == Double.class
                       || clazz == Void.class;

    }
}
