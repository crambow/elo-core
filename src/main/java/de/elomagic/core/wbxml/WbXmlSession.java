/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class will be used during decoding and encoding of XML.
 *
 * @author Carsten Rambow
 */
public class WbXmlSession {

    public WbXmlSession(Set<AbstractCodePage> codePages) {
        codePages
                .stream()
                .forEach(cp->codePageMap.put(cp.getCodePageId(), cp));

        currentCodePage = codePageMap.get(0);
    }

    private final Map<Integer, AbstractCodePage> codePageMap = new HashMap<>();
    private AbstractCodePage currentCodePage;
    private Charset charset = StandardCharsets.UTF_8;

    public int getCurrentCodePage() {
        return currentCodePage.getCodePageId();
    }

    public void setCurrentCodePage(int currentCodePageId) throws Exception {
        if(!codePageMap.containsKey(currentCodePageId)) {
            throw new Exception("Code page id \"" + currentCodePageId + "\" not supported.");
        }

        currentCodePage = codePageMap.get(currentCodePageId);
    }

    public Charset getCharset() {
        return charset;
    }

    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    public boolean hasToChangeCodePage(String tagName) {
        return !currentCodePage.containsTagName(tagName);
    }

    public int getCodePageId(String tagName) throws Exception {
        int codePage = codePageMap
                .values()
                .stream()
                .filter(cp->cp.containsTagName(tagName))
                .findFirst()
                .orElseThrow(()->new Exception("Tag name \"" + tagName + "\" not supported."))
                .getCodePageId();

        return codePage;
    }

    public int getToken(String tagName) throws Exception {
        int token = currentCodePage.getToken(tagName);

        return token;
    }

    public String getTagName(int token) throws Exception {
        String tagName = currentCodePage.getTagName(token);

        return tagName;
    }

}
