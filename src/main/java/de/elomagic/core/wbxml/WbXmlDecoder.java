/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.text.StringEscapeUtils;

/**
 *
 * @author Carsten Rambow
 */
public class WbXmlDecoder {

    public static <T> T decode(Class<? extends T> clazz, byte[] bytes, Set<AbstractCodePage> codePages) throws Exception {
        return decode(clazz, new ByteArrayInputStream(bytes), codePages);
    }

    public static <T> T decode(Class<? extends T> clazz, InputStream in, Set<AbstractCodePage> codePages) throws Exception {
        byte[] bytes = decodeWbXmlToXml(in, codePages);

        try (InputStream is = new ByteArrayInputStream(bytes); Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8)) {
            JAXBContext context = JAXBContext.newInstance(clazz);
            Unmarshaller u = context.createUnmarshaller();

            T o = (T)u.unmarshal(reader);

            return o;
        }
    }

    /**
     * Decode an WbXml encoded input stream to a XML encoded input stream.
     *
     * @param in Input stream with the encoded object
     * @param codePages Code page to be applied
     * @return Returns an input stream with XML content.
     * @throws IOException Thrown when unable to read encoded object from input stream
     */
    public static byte[] decodeWbXmlToXml(InputStream in, Set<AbstractCodePage> codePages) throws Exception {

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WbXmlSession session = new WbXmlSession(codePages);

        byte[] header = new byte[3];
        in.read(header);
        session.setCharset(getDecodedCharset(header[2]));

        out.write(("<?xml version='1.0' encoding='" + session.getCharset().name() + "' ?>").getBytes());

        int stringLength = in.read();

        out.write(parse(in, session));

        //LOGGER.trace(new String(b));
        return out.toByteArray();
    }

    private static byte[] parse(InputStream in, WbXmlSession session) throws Exception {
        // TODO Attributes currently no supported

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        int nextByte;

        while((nextByte = in.read()) != WbXmlDefaultToken.END && (nextByte != -1)) {
            if(nextByte == WbXmlDefaultToken.SWITCH_PAGE) {
                int cp = in.read();
                session.setCurrentCodePage(cp);
            } else if((nextByte & WbXmlDefaultToken.EXT_I_0) == WbXmlDefaultToken.EXT_I_0) {
                String elementName = session.getTagName(nextByte - WbXmlDefaultToken.EXT_I_0);
                out.write(("<" + elementName + ">").getBytes());
                out.write(parse(in, session));
                out.write(("</" + elementName + ">").getBytes());
                //in.read();
            } else if(nextByte == WbXmlDefaultToken.STR_I) {
                out.write(readString(in, session.getCharset()));
            } else if(nextByte > WbXmlDefaultToken.LITERAL && nextByte < WbXmlDefaultToken.EXT_I_0) {
                // No content
                String elementName = session.getTagName(nextByte);
                out.write(("<" + elementName + "/>").getBytes());
            } else {
                throw new Exception("Unsupported byte \"" + nextByte + "\" read.");
            }
        }

        return out.toByteArray();
    }

    private static byte[] readString(InputStream in, Charset charset) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int b;
        while((b = in.read()) != 0x00) {
            out.write(b);
        }

        String original = new String(out.toByteArray(), charset);
        String escaped = StringEscapeUtils.escapeXml10(original);
        return escaped.getBytes(charset);
    }

    /**
     * @see https://www.iana.org/assignments/character-sets/character-sets.xhtml
     * @param charset
     * @return
     * @throws IOException Thrown when unable char not supported
     */
    private static Charset getDecodedCharset(byte charset) throws IOException {

        switch(charset) {
            case 0x6a:
                return StandardCharsets.UTF_8;
            case 0x04:
                return StandardCharsets.ISO_8859_1;
            case 0x03:
                return StandardCharsets.US_ASCII;

            default:
                throw new IOException("Charset value \"" + charset + "\" not supported");
        }

    }

}
