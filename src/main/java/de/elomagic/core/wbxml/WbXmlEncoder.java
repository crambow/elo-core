/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Carsten Rambow
 */
public final class WbXmlEncoder {

    private WbXmlEncoder() {
    }

    /**
     * Encode a XML annotated object to a WbXML encoded byte array.
     *
     * @param object Object to encode
     * @param codePages Code page to be applied
     * @return A byte array but never null
     * @throws Exception Thrown when unable to encode given object
     */
    public static byte[] encodeObject(Object object, Set<AbstractCodePage> codePages) throws Exception {

        InputStream in = encodeObjectToXml(object);

        return encode(in, codePages);
    }

    /**
     * Encode a XML documented input stream into a WbXML encoded byte array.
     *
     * @param in Input stream of the object to be encoded
     * @param codePages Code page to be applied
     * @return A byte array but never null
     * @throws Exception Thrown when encoding of the object given by the input stream failed
     */
    public static byte[] encode(InputStream in, Set<AbstractCodePage> codePages) throws Exception {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(in);

        byte[] bytes = encodeDocumentToWbXml(doc, codePages);

        return bytes;
    }

    /**
     * Write a Simple XML annotated object to an writer stream.
     * <p>
     * @param o Simple XML Annotated object
     * @throws JAXBException Thrown when marshalling of the object failed.
     */
    private static InputStream encodeObjectToXml(final Object o) throws JAXBException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        JAXBContext context = JAXBContext.newInstance(o.getClass());
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.setProperty(Marshaller.JAXB_ENCODING, "utf-8");
        m.marshal(o, out);

        return new ByteArrayInputStream(out.toByteArray());
    }

    private static byte[] encodeDocumentToWbXml(Document doc, Set<AbstractCodePage> codePages) throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WbXmlSession session = new WbXmlSession(codePages);

        // Write header
        out.write(0x03);
        // Unknown publisher
        out.write(0x01);
        out.write(getEncodedCharset(session.getCharset()));

        // String table length
        out.write(0x00);

        parse(out, doc.getDocumentElement(), session);

        return out.toByteArray();
    }

    private static void parse(final OutputStream out, final Element e, WbXmlSession session) throws Exception {
        String elementName = e.getNodeName();
        if(session.hasToChangeCodePage(elementName)) {
            int requiredCodePage = session.getCodePageId(elementName);
            session.setCurrentCodePage(requiredCodePage);
            out.write(WbXmlDefaultToken.SWITCH_PAGE);
            out.write(requiredCodePage);
        }

        final NodeList children = e.getChildNodes();

        int token = session.getToken(elementName);

        if(children.getLength() == 1 && children.item(0).getNodeType() == Node.TEXT_NODE) {
            token += WbXmlDefaultToken.EXT_I_0;
            out.write(token);

            writeInlineString(out, e.getTextContent(), session);

            out.write(WbXmlDefaultToken.END);
        } else if(children.getLength() != 0) {
            // TODO Currently no XML attributes supported

            token += WbXmlDefaultToken.EXT_I_0;
            out.write(token);

            for(int i = 0; i < children.getLength(); i++) {
                final Node n = children.item(i);
                if(n.getNodeType() == Node.ELEMENT_NODE) {
                    //list.add(n.getNodeName());
                    parse(out, (Element)n, session);
                    //} else if(n.getNodeType() == Node.TEXT_NODE) {
                    //    writeInlineString(out, n.getNodeValue());
                }
            }

            // Closing element
            out.write(WbXmlDefaultToken.END);
        } else {
            // Contains no content
            out.write(token);
        }
    }

    private static void writeInlineString(final OutputStream out, String value, WbXmlSession session) throws IOException {
        out.write(WbXmlDefaultToken.STR_I);
        out.write(value.getBytes(session.getCharset()));
        out.write(0x00);
    }

    /**
     * @see https://www.iana.org/assignments/character-sets/character-sets.xhtml
     * @param charset
     * @return
     * @throws Exception Thrown when something going wrong.
     */
    private static byte getEncodedCharset(Charset charset) throws Exception {

        if(StandardCharsets.UTF_8.equals(charset)) {
            return 0x6a;
        } else if(StandardCharsets.ISO_8859_1.equals(charset)) {
            return 0x04;
        } else if(StandardCharsets.US_ASCII.equals(charset)) {
            return 0x03;
        }

        throw new Exception("Charset \"" + charset + "\" not supported");
    }

}
