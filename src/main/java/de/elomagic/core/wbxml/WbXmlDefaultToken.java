/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml;

/**
 *
 * @author Carsten Rambow
 */
public final class WbXmlDefaultToken {

    private WbXmlDefaultToken() {
    }

    public static final byte SWITCH_PAGE = 0x00;
    public static final byte END = 0x01;
    public static final byte ENTITY = 0x02;
    public static final byte STR_I = 0x03;
    public static final byte LITERAL = 0x04;
    public static final byte EXT_I_0 = 0x40;

}
