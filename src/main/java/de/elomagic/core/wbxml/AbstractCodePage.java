/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.wbxml;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Carsten Rambow
 */
public abstract class AbstractCodePage {

    public abstract int getCodePageId();

    public abstract Map<Integer, String> getTagMap();

    public boolean containsToken(int token) {
        return getTagMap().containsKey(token);
    }

    public String getTagName(int token) {
        return getTagMap().get(token);
    }

    public boolean containsTagName(String tagName) {
        return getTagMap().values().contains(tagName);
    }

    public int getToken(String tagName) {
        Map<String, Integer> map = new HashMap();
        getTagMap()
                .entrySet()
                .stream()
                .forEach(entry->map.put(entry.getValue(), entry.getKey()));

        return map.get(tagName);
    }

}
