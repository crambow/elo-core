/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.app;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;

/**
 * Reads properties from the resource file <code>pom.properties</code> or <code>MANIFEST.MF</code>.
 *
 * @author Carsten Rambow
 */
public abstract class AbstractApplicationProperties {

    private static final Logger LOGGER = LogManager.getLogger(AbstractApplicationProperties.class);

    public AbstractApplicationProperties() {
    }

    /**
     *
     * @return Returns something like <code>/META-INF/maven/de.elomagic/j2c-bridge/pom.xml</code>.
     */
    public abstract String getPomXmlName();

    /**
     *
     * @return Returns something like <code>/META-INF/maven/de.elomagic/j2c-bridge/pom.properties</code>.
     */
    public abstract String getPomPropertiesName();

    private InputStream getResource(String name) throws IOException {
        InputStream in = AbstractApplicationProperties.class.getResourceAsStream(name);

        if(in == null) {
            throw new IOException("Resource file \"" + name + "\" doesn't exists. Do you running application outside from the JAR?");
        }

        return in;
    }

    private Document getPomXml() {
        Document document = null;
        try (InputStream in = getResource(getPomXmlName())) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse(in);
        } catch(Exception ex) {
            LOGGER.warn(ex.getMessage(), ex);
        }

        return document;
    }

    private Properties getPomProperties() {
        Properties properties = new Properties();
        try {
            properties.load(getResource(getPomPropertiesName()));
        } catch(Exception ex) {
            LOGGER.warn(ex.getMessage(), ex);
        }

        return properties;
    }

    private String getPomXmlValue(String expr, String defaultValue) {
        String value = defaultValue;

        Document document = getPomXml();
        if(document != null) {
            try {
                XPathFactory xPathfactory = XPathFactory.newInstance();
                XPath xpath = xPathfactory.newXPath();
                XPathExpression pe = xpath.compile(expr);
                value = (String)pe.evaluate(document, XPathConstants.STRING);
            } catch(Exception ex) {
                LOGGER.warn(ex.getMessage(), ex);
            }
        }

        return value;
    }

    /**
     * Returns property "application.version" of the "pom.properties" files.
     *
     * @return Returns property "application.version"
     */
    public String getVersion() {
        return getPomProperties().getProperty("version", "unknown");
    }

    /**
     * Returns property "project.name" of the "pom.xml" files.
     *
     * @return Returns property "project.name"
     */
    public String getName() {
        return getPomXmlValue("/project/name", "unknown");
    }

    /**
     * Returns presentable version string.
     * <p>
     * Terminate with build timestamp when version itself ends with string "-SNAPSHOT".
     *
     * @return Returns presentable version string.
     */
    public String getDisplayVersion() {

        String version = getVersion();

        if(version.endsWith("-SNAPSHOT")) {
            version += " (build timestamp is unknown)";
        }
        return version;
    }

    /**
     * Returns displayable application name and version.
     *
     * @return Something like "Name 1.0.0.1";
     */
    public String getDisplayNameVersion() {
        return getName() + " " + getDisplayVersion();
    }

}
