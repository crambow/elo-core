/*
 * elomagic Core (Java 11)
 * Copyright (c) 2017-present Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.elomagic.core.settings;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.util.StringUtils;

import de.elomagic.core.settings.annotations.SettingsProperty;
import de.elomagic.core.settings.annotations.SettingsRoot;
import de.elomagic.core.utils.ClassUtils;
import de.elomagic.core.utils.SimpleCrypt;

/**
 *
 * @param <T> Class of root settings element
 */
public abstract class Settings<T> {

    private static final Logger LOGGER = LogManager.getLogger(Settings.class);

    private List<Object> collectedFields = new ArrayList<>();
    private Path configurationFile;
    private T root;

    protected abstract Class<T> getRootClass();

    public T getRoot() {
        return root;
    }

    /**
     * Returns encryption mode when read a configuration file
     *
     * @return Returns true when field will be encrypted and written automatically after successful read.
     */
    public boolean isAutoEncrypt() {
        return false;
    }

    public void clear() throws SettingsException {
        try {
            root = getRootClass().getConstructor().newInstance();
        } catch (Exception ex) {
            throw new SettingsException(ex.getMessage(), ex);
        }
    }

    public void read(Reader reader) {
        Gson gson = new Gson();
        root = gson.fromJson(reader, getRootClass());
    }

    public void read(Path file) throws Exception {
        if(!file.toFile().exists()) {
            throw new NoSuchFileException(file.toString(), null, "File \"" + file + "\" doesn't exists.)");
        }

        configurationFile = file;

        LOGGER.debug("Reading configuration file \"{}\".", file);
        try (Reader reader = Files.newBufferedReader(file, StandardCharsets.UTF_8)) {
            read(reader);
        }

        // If configuration contains unprotected passwords then protect them afterwards if defined by annotation.
        SettingsRoot settingsRoot = getSettingsRootAnnotation();
        if(hasUnprotectedValues(root)
                   && (settingsRoot != null && settingsRoot.encryptPasswords()
                       || settingsRoot == null && isAutoEncrypt())) {
            write();
        }

        LOGGER.info("Configuration file \"{}\" successful read.", file);
    }

    /**
     * Write settings to file which was given with method {@link Settings#read(java.nio.file.Path) }.
     *
     * @throws IOException Thrown when unable to handle the file.
     */
    public void write() throws IOException {
        LOGGER.debug("Writing configuration file \"{}\".", configurationFile);

        if(configurationFile == null) {
            throw new IOException("Configuration file not defined.");
        }

        try {
            encodeSecureFields(root);

            try (Writer writer = Files.newBufferedWriter(configurationFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
                Gson gson = new GsonBuilder()
                        .setPrettyPrinting()
                        .create();
                gson.toJson(root, writer);
            }
        } catch(Exception ex) {
            throw new IOException(ex);
        }

        LOGGER.debug("Configuration file \"{}\" successful written.", configurationFile);
    }

    public void writeDefault() throws SettingsException {
        try {
            SettingsRoot settingsRoot = getSettingsRootAnnotation();
            if (settingsRoot != null && StringUtils.hasText(settingsRoot.templateResource())) {
                if (!configurationFile.getParent().toFile().exists()) {
                    Files.createDirectories(configurationFile.getParent());
                }

                if (configurationFile.toFile().exists()) {
                    LOGGER.debug("Configuration file \"{}\" already exists. Overwrite old one.", configurationFile);
                }

                // Read default from resource and write as default.
                Files.copy(getClass().getResourceAsStream(settingsRoot.templateResource()), configurationFile);
            }
        } catch (Exception ex) {
            throw new SettingsException(ex.getMessage(), ex);
        }
    }

    private SettingsRoot getSettingsRootAnnotation() {
        SettingsRoot settingsRoot = getRootClass().getAnnotation(SettingsRoot.class);

        Class<?> c = getRootClass();
        while(settingsRoot == null && (c = c.getSuperclass()) != null) {
            settingsRoot = c.getAnnotation(SettingsRoot.class);
        }

        return settingsRoot;
    }

    /**
     * Returns a set of field with annotation {@link SettingsProperty#secure() } is set.
     *
     * @param parent Fields of parent to be checked
     * @return Returns a set but never null
     * @throws Exception Thrown when unable to to collect fields but can't be handled by itself.
     */
    private Set<FieldInstance> collectSecureFields(Object parent) throws Exception {
        LOGGER.trace("Parent: {}", parent);

        if(parent == null) {
            return Collections.emptySet();
        }

        if (collectedFields.contains(parent)) {
            LOGGER.warn("An invalid cycle was detected when checking the configuration. Please check code implementation! Parent: {}", parent.getClass().getName());
            return Collections.emptySet();
        }

        collectedFields.add(parent);
        Set<FieldInstance> result = new HashSet<>();

        Set<Field> fields = getInheritedFields(parent.getClass());
        for(Field field : fields) {
            Class<?> clazz = field.getType();

            if(ClassUtils.isPrimitiveType(clazz)) {
                continue;
            }

            if (clazz.isEnum()) {
                LOGGER.trace("Field \"{}\" is of class type \"enum\". Skip and continue with iteration.", clazz.getName());
                continue;
            }

            if(clazz == String.class) {
                if(field.isAnnotationPresent(SettingsProperty.class)) {
                    SettingsProperty annotation = field.getAnnotation(SettingsProperty.class);
                    if(annotation.secure()) {
                        result.add(new FieldInstance(parent, field));
                    }
                }
            } else if(Iterable.class.isAssignableFrom(clazz)) {
                boolean isAccessable = field.canAccess(parent);
                field.setAccessible(true);
                try {
                    Iterable<?> childs = (Iterable<?>)field.get(parent);
                    for(Object child : childs) {
                        Set<FieldInstance> s = collectSecureFields(child);
                        result.addAll(s);
                    }
                } finally {
                    field.setAccessible(isAccessable);
                }
            } else {
                boolean isAccessable = field.canAccess(parent);
                field.setAccessible(true);
                try {
                    Object o = field.get(parent);
                    Set<FieldInstance> s = collectSecureFields(o);
                    result.addAll(s);
                } finally {
                    field.setAccessible(isAccessable);
                }
            }
        }

        return result;
    }

    protected Set<Field> getInheritedFields(Class<?> clazz) {

        Field[] fields = clazz.getDeclaredFields();
        Set<Field> result = new HashSet<>(Arrays.asList(fields));
        Class<?> c = clazz;
        while((c = c.getSuperclass()) != null) {
            fields = c.getDeclaredFields();
            result.addAll(Arrays.asList(fields));
        }

        return result;
    }

    /**
     * Returns status of unprotected properties.
     * <p>
     * Checks all {@link  Field} recursively of the given object.
     *
     * @return Returns true when at least on property which is annotated as secure but not encrypted
     * @throws Exception Thrown when unable to check protection
     */
    public boolean hasUnprotectedValues() throws Exception {
        return hasUnprotectedValues(root);
    }

    /**
     * Returns status of unprotected properties.
     * <p>
     * Checks all {@link  Field} recursively of the given object.
     *
     * @param parent Node of all child leafs (properties) to check
     * @return Returns true when at least on property which is annotated as secure but not encrypted
     * @throws Exception Thrown when unable to check protection
     */
    private boolean hasUnprotectedValues(Object parent) throws Exception {
        collectedFields.clear();
        Set<FieldInstance> set = collectSecureFields(parent);

        return set
                .stream()
                .filter(item->SimpleCrypt.isFieldValueUnprotected(item.getField(), item.getParent()))
                .findFirst()
                .orElse(null) != null;
    }

    private void encodeSecureFields(Object parent) throws Exception {
        collectedFields.clear();
        Set<FieldInstance> set = collectSecureFields(parent);

        set
                .stream()
                .filter(item->SimpleCrypt.isFieldValueUnprotected(item.getField(), item.getParent()))
                .forEach(item->SimpleCrypt.encryptFieldValue(item.getField(), item.getParent()));
    }

}
